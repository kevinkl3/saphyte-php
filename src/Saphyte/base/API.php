<?php

namespace Saphyte\base;

use Saphyte\Workspace;
use GuzzleHttp;

/**
 * Class to perform HTTP Requests to the Saphyte API
 */
class API extends BaseService{

    protected $baseURL;
    protected $client;
    protected $behaviour;


    const DONT_PARSE_RESPONSES  = 0;
    const PARSE_RESPONSES       = 100;

    const BEHAVIOURS = [
        self::DONT_PARSE_RESPONSES,
        self::PARSE_RESPONSES,
    ];

    public function __construct($baseAPIURL){
        $this->baseURL = $baseAPIURL;
        $this->client = new GuzzleHttp\Client(['base_uri' => $baseAPIURL]);
        $this->behaviour = self::DONT_PARSE_RESPONSES;
    }

    /**
     * Builds an instance of the API with the
     * default configuration
     * @return API
     */
    public static function build(){
        return new API(Workspace::getAPIURL());
    }

    /**
     * Performs a GET request to the specified path
     *
     * @param string $path
     * @return any
     */
    public function GET($path){
        $response = $this->client->request('GET', $path);
        return $this->processResponse($response);
    }

    /**
     * Performs a POST request to the specified path
     *
     * @param string $path
     * @param array $postData
     * @return object
     */
    public function POST($path,$postData){
        $response = $this->client->request('POST', $path, [
            'form_params' => $postData
        ]);
        return $this->processResponse($response);
    }

    /**
     * Changes the behaviour of the API
     *  API::PARSE_RESPONSES or API::DONT_PARSE_RESPONSES
     * @param [type] $behaviour
     * @return void
     */
    public function setBehaviour($behaviour){
        if(!in_array($behaviour,self::BEHAVIOURS)){
            throw new \Exception("Invalid behaviour '" . $behaviour . "'");
        }
        $this->behaviour = $behaviour;
    }


    /**
     * Process the responce according to the configured API Behaviour
     *
     * @param object $response
     * @return any
     */
    protected function processResponse($response){
        if($this->behaviour == self::PARSE_RESPONSES){
            $body = (string)$response->getBody();
            return json_decode($body,true);
        }
        return $response;
    }
}