<?php

namespace Saphyte\base;

/**
 * BaseService defines the basic functions
 * and fields for a service.
 * Error handling
 */
abstract class BaseService {

    protected $errors = [];

    /**
     * hasErrors returns true if the service has errors,
     * false otherwise
     * @return boolean
     */
    public function hasErrors() {
		return count($this->errors) > 0;
	}

    /**
     * addError adds an error to the service
     *
     * @param string $errorMsg
     * @return void
     */
	protected function addError($errorMsg) {
		$this->errors[] = $errorMsg;
	}

    /**
     * addErrors add the given errors to the service
     *
     * @param array(string) $errors
     * @return void
     */
	protected function addErrors($errors) {
        if(is_null($errors)){
            $errors = [];
        }
		$this->errors = array_merge($this->errors, $errors);
	}

    /**
     * getErrors returns the errors captured by the service
     *
     * @return array(string)
     */
	public function getErrors() {
		return $this->errors;
	}

    /**
     * clearErrors removes any captured error
     *
     * @return array(string)
     */
	public function clearErrors(){
		$errs = $this->errors;
		$this->errors = [];
		return $errs;
	}
}