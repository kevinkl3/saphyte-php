<?php
namespace Saphyte\forms;

use Saphyte\base\Utils;
/**
 * FormSubmission represents a Saphyte Form Submission
 */
class FormSubmission {

    protected $clientIP;
    protected $data;
    protected $formID;
    protected $referredBy;

    function __construct($formID,$formData=null){
        $this->formID = $formID;
        if($formData == null){
            $formData = [];
        }
        $this->data = $formData;
    }

    /**
     * Sets the value for the specified field
     *
     * @param string $fieldName
     * @param any $value
     * @return void
     */
    public function setFieldValue($fieldName,$value){
        $this->data[$fieldName] = $value;
    }

    /**
     * Sets the values for the specified fields
     * [
     *     'name' => 'Jane',
     *     'age' => 20,
     * ]
     * @param array $fieldValuesMap
     * @return void
     */
    public function setFieldValues($fieldValuesMap){
        foreach($fieldValuesMap as $field => $value){
            $this->data[$field] = $value;
        }
    }

    /**
     * Sets the client IP address
     *
     * @param string $ip
     * @return void
     */
    public function setClientIP($ip){
        $this->clientIP = $ip;
    }

    /**
     * Sets the referral ID
     *
     * @param [type] $referalID
     * @return void
     */
    public function setReferredBy($referralID){
        $this->referredBy = $referralID;
    }

    /**
     * returns the form parameters to be submited
     *
     * @return void
     */
    public function getFormParams(){
        if(!isset($this->clientIP)){
            $this->clientIP = Utils::getClientIP();
        }
        return [
            'form' => $this->formID,
            'data' => $this->data,
            'client_ip' => $this->clientIP,
            'referred_by' => $this->referredBy,
        ];
    }

}