<?php
namespace Saphyte;

/**
 * Represents a Saphyte Workspace
 */
class Workspace{
    protected static $name;

    /**
     * Initializes a workspace based on the given name
     *
     * @param string $workspaceName
     * @return void
     */
    public static function init($workspaceName){
        static::$name = $workspaceName;
    }

    /**
     * Gets the base API URL for the workspace
     *
     * @return string
     */
    public static function getAPIURL(){
        if(!isset(static::$name)){
            throw new \Exception("Workspace is not initialized");
        }
        $subdomain = static::$name;
        return "https://$subdomain.saphyteapi.com";
    }
}