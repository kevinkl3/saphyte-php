<?php

namespace Saphyte\service;

use Saphyte\base\API;
use Saphyte\base\BaseService;
use Saphyte\forms\FormSubmission;

class FormService extends BaseService{
    protected $api;
    
    public function __construct(){
        $this->api = API::build();
        $this->api->setBehaviour(API::PARSE_RESPONSES);
    }

    /**
     * Performs a form submission
     *
     * @param FormSubmission $formSubmission
     * @return void
     */
    public function submit(FormSubmission $formSubmission){
        return $this->api->POST('/form/submit',$formSubmission->getFormParams());
    }

}